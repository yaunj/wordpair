package main

import (
	"encoding/json"
	"fmt"
	"html"
	"net/http"

	"github.com/gorilla/mux"
)

var jsonMime = "application/json"

func returnJSON(r *http.Request) bool {
	for _, accept := range r.Header["Accept"] {
		if accept == jsonMime {
			return true
		}
	}

	for _, ct := range r.Header["Content-Type"] {
		if ct == jsonMime {
			return true
		}
	}

	return false
}

func writePhraseResponse(w http.ResponseWriter, r *http.Request, p Phrase) {
	// Provide http status code
	if p.Phrase != "" {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}

	// Json for json, plain text if not
	if returnJSON(r) {
		w.Header().Set("Content-Type",
			"application/json; charset=UTF-8")

		json.NewEncoder(w).Encode(p)
	} else {
		w.Header().Set("Content-Type",
			"text/plain; charset=UTF-8")
		fmt.Fprintf(w, "Hello, %q\n", html.EscapeString(r.URL.Path))
		fmt.Fprintf(w, p.Phrase)
	}
}

// EnglishWords provides random english words
func EnglishWords(w http.ResponseWriter, r *http.Request) {
	phrase := GetWords()

	fmt.Fprintf(w, "Hello, %q\n", html.EscapeString(r.URL.Path))
	fmt.Fprintf(w, "phrase: %s\n", phrase.Phrase)
}

// MultipleEnglishWords provides multiple random english words
func MultipleEnglishWords(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	words := GetMultipleWords(vars["classes"])

	writePhraseResponse(w, r, words)
}

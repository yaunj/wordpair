wordpair
========

This is a go version of
[python-wordpair](https://gitlab.com/yaunj/python-wordpair).

Test it with:

    docker run --rm -p5050:5050 registry.gitlab.com/yaunj/wordpair

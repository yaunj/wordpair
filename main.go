package main

import (
	"log"
	"math/rand"
	"net/http"
	"time"
)

var mobydict Dictionary
var rng *rand.Rand

func init() {
	mobydict = parseMobyDict("mpos/mobyposi.i")

	seed := rand.NewSource(time.Now().UnixNano())
	rng = rand.New(seed)
}

func main() {
	log.Println(log.Prefix())
	log.Println(log.Flags())
	log.Println("Starting up:", GetWords().Phrase)

	router := CreateRouter()

	log.Fatal(http.ListenAndServe(":5050", router))
}

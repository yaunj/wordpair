FROM scratch

ADD ["wordpair", "/"]
ADD ["mpos/mobyposi.i", "/mpos/mobyposi.i"]
EXPOSE 5050

CMD ["/wordpair"]

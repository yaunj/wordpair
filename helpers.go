package main

import (
	"bufio"
	"bytes"
	"log"
	"os"
	"strings"

	"golang.org/x/text/encoding/charmap"
)

func parseMobyDict(path string) Dictionary {
	dict := make(Dictionary)
	decoder := charmap.Macintosh.NewDecoder()

	recordSeparator := byte("\r"[0])
	fieldSeparator := []byte("\xd7")

	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	reader := bufio.NewReader(file)

	record, recordErr := reader.ReadBytes(recordSeparator)

	for recordErr == nil {
		pair := bytes.Split(record, fieldSeparator)

		if len(pair) < 2 {
			continue
		}

		rawWord, err := decoder.Bytes(pair[0])
		if err != nil {
			log.Printf("Problem with %v", pair[0])
			continue
		}
		word := string(rawWord)
		classes := pair[1]

		for _, class := range bytes.Split(classes, []byte("")) {
			key := mobyMapping[rune(class[0])]
			dict[key] = append(dict[key], string(word))
		}

		record, recordErr = reader.ReadBytes(recordSeparator)
	}

	file.Close()

	return dict
}

// GetWords returns a random Adverb and a random Noun, and a phrase combining
// them
func GetWords() Phrase {
	noun := mobydict[Noun][rng.Intn(len(mobydict[Noun]))]
	adverb := mobydict[Adverb][rng.Intn(len(mobydict[Adverb]))]
	words := Words{Word{Adverb, adverb}, Word{Noun, noun}}

	return Phrase{
		words,
		strings.Join([]string{words[0].Word, words[1].Word}, " "),
	}
}

// GetMultipleWords returns multiple random words, and a phrase combining them
func GetMultipleWords(classes string) Phrase {
	words := make([]Word, 0)
	sentence := make([]string, 0)

	for _, char := range classes {
		wordclass, err := mobyMapping[char]
		if !err {
			return Phrase{}
		}

		haystack := mobydict[wordclass]
		word := Word{
			wordclass,
			haystack[rng.Intn(len(haystack))],
		}
		words = append(words, word)
		sentence = append(sentence, word.Word)
	}

	return Phrase{
		words,
		strings.Join(sentence, " "),
	}
}

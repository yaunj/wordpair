package main

import (
	"net/http"
)

// The Route type defines a route in this app
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// Routes is an array of Route structs
type Routes []Route

var routes = Routes{
	Route{"Root", "GET", "/", EnglishWords},
	Route{"English", "GET", "/en/", EnglishWords},
	Route{"EnglishMulti", "GET", "/en/{classes}", MultipleEnglishWords},
}

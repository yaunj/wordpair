package main

import (
	"github.com/gorilla/mux"
)

// CreateRouter creates  a new router by collecting the routes in routes.go
func CreateRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)

	for _, r := range routes {
		handler := r.HandlerFunc
		handler = Logger(handler, r.Name)

		router.
			Methods(r.Method).
			Path(r.Pattern).
			Name(r.Name).
			Handler(handler)
	}

	return router
}

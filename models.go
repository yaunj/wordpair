package main

// A collection of constants for the different word classes
const (
	Noun              = "Noun"
	Plural            = "Plural"
	NounPhrase        = "NounPhrase"
	Verb              = "Verb"
	VerbTransitive    = "VerbTransitive"
	VerbIntransitive  = "VerbIntransitive"
	Adjective         = "Adjective"
	Adverb            = "Adverb"
	Conjunction       = "Conjunction"
	Preposition       = "Preposition"
	Interjection      = "Interjection"
	Pronoun           = "Pronoun"
	ArticleDefinite   = "ArticleDefinite"
	ArticleIndefinite = "ArticleIndefinite"
	Nominative        = "Nominative"
)

var mobyMapping = map[rune]Wordclass{
	'N': Noun,
	'p': Plural,
	'h': NounPhrase,
	'V': Verb,
	't': VerbTransitive,
	'i': VerbIntransitive,
	'A': Adjective,
	'v': Adverb,
	'C': Conjunction,
	'P': Preposition,
	'!': Interjection,
	'r': Pronoun,
	'D': ArticleDefinite,
	'I': ArticleIndefinite,
	'o': Nominative,
}

// A Wordclass type
type Wordclass string

// A Dictionary type maps a Wordclass to an array of word strings
type Dictionary map[Wordclass][]string

// A Word type specifies both the word and the word class
type Word struct {
	Wordclass Wordclass `json:"class"`
	Word      string    `json:"word"`
}

// The Words type is an array of Word structs
type Words []Word

// A Phrase is a collection of words and a string combining them
type Phrase struct {
	Words  Words  `json:"words"`
	Phrase string `json:"phrase"`
}
